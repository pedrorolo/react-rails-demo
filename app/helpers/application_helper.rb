module ApplicationHelper
  def file_caption file, slice: nil
    render partial: 'file_caption', locals: {file: file, slice: slice}
  end

  def component_caption component, slice: nil
    render partial: 'file_caption', locals: {component: component, slice: slice}
  end

  def view_caption view, slice: nil
    file_caption "#{Rails.root}/app/views/#{view}", slice: slice
  end

  def controller_caption controller, slice: nil
    file_caption "#{Rails.root}/app/controllers/#{controller}", slice: slice
  end

end
