class RecordsController < ApplicationController
  def index
    @records = Record.all
  end

  def records1
    @records = Record.all
    @next_path = records2_records_path
  end

  def records2
    @records = Record.all
    @next_path = records3_records_path
  end

  def records3
    @records = Record.all
    @next_path = records4_records_path
  end

  def records4
    @records = Record.all
    @next_path = records5_records_path
  end

  def records5
    @records = Record.all
    @next_path = records_path
  end


  def create
    @record = Record.new(record_params)

    if @record.save
      render json: @record
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    head :no_content
  end

  def update
    @record = Record.find(params[:id])
    if @record.update(record_params)
      render json: @record
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  private

  def record_params
    params.require(:record).permit(:title, :amount, :date)
  end
end
