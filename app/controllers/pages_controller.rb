class PagesController < ApplicationController

  def index
  end

  def hello_jsx
    @name = "JSX World"
    @next_path = hello_js_pages_path
  end

  def hello_js
    @name = "JS World"
    @next_path = hello_world_pages_path
  end

  def hello_world
    @name = "Coffee World"
    @next_path = hello_with_input_pages_path
  end

  def hello_with_input
    @name = "Changeable World"
    @next_path = records1_records_path
  end
end
