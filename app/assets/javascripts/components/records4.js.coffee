class @Records4 extends React.Component
  constructor: (@props)->
    @state =
      records: @props.data || []

  render: ->
    {div, h2, table, thead, tbody, tr, th, br, hline} = React.DOM
    createElement = React.createElement
    div
      className: 'records'
      h2
        className: 'title'
        'Records'
      table
        className: 'table table-bordered',
        thead {},
          tr {},
            th {}, 'Title'
            th {}, 'Amount'
            th {}, 'Actions'
        tbody {},
            @state.records.map (record) =>
#             #pass edit record callback
              createElement Record,
                key: record.id
                record: record
                handleDeleteRecord: @deleteRecord.bind(this),
                handleEditRecord: @editRecord.bind(this)
      hline
      createElement RecordForm, handleNewRecord: @addRecord.bind(this)

# #the callback itself
  editRecord: (record, data) ->
    newRecords = @state.records.map (r) ->
      if record.id == r.id
        data
      else
        r
    @setState records: newRecords

  addRecord: (record) ->
    @setState records: @state.records.concat(record)

  deleteRecord: (record)->
    newRecords = @state.records.filter (r) ->
      r.id != record.id
    @setState records: newRecords
