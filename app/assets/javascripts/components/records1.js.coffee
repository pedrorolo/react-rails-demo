class @Records1 extends React.Component
  render: ->
    {div, h2, table, thead, tbody, tr, th} = React.DOM
    createElement = React.createElement
    div {className: 'records'},
      h2 {className: 'title'},
        'Records'
      table {className: 'table table-bordered'},
        thead {},
          tr {},
            th {}, 'Title'
            th {}, 'Amount'
          @props.data.map (record) ->
            createElement Record1,
              key: record.id
              record: record
