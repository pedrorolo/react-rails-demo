class @Records3 extends React.Component
  constructor: (@props)->
    @state = {records: @props.data || []}

  render: ->
    {div, h2, table, thead, tbody, tr, th, br, hline} = React.DOM
    createElement = React.createElement
    div
      className: 'records'
      h2
        className: 'title'
        'Records'
      table
        className: 'table table-bordered',
        thead {},
          tr {},
            th {}, 'Title'
            th {}, 'Amount'
#           #new column
            th {}, 'Actions'
#       #passing the delete callback
        tbody {},
          @state.records.map (record) =>
            createElement Record3,
              key: record.id
              record: record
              handleDeleteRecord: @deleteRecord.bind(this)
      hline
      createElement RecordForm, handleNewRecord: @addRecord.bind(this)

  addRecord: (record) ->
    @setState records: @state.records.concat(record)

# #the delete callback
  deleteRecord: (record)->
    newRecords = @state.records.filter (r) ->
      return r.id != record.id
    @setState records: newRecords
