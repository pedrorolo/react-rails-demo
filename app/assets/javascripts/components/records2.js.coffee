class @Records2 extends React.Component
# #changed
  constructor: (@props)->
    @state =
      records: @props.data || []

  render: ->
    {div, h2, table, thead, tbody, tr, th, br, hline} = React.DOM
    createElement = React.createElement
    div {className: 'records'},
      h2 {className: 'title'},
        'Records'
      table {className: 'table table-bordered'},
        thead {},
          tr {},
            th {}, 'Title'
            th {}, 'Amount'
          @state.records.map (record) ->
            createElement Record1,
              key: record.id
              record: record
      hline
#     #passing the callback to the record form
      createElement RecordForm,
        handleNewRecord: @addRecord.bind(this)

# #the callback itself
  addRecord: (record) ->
    @setState records: @state.records.concat(record)
