class @Record extends React.Component
  constructor: (@props)->
    @state = { edit: false }

# #render depending on the edit state
  render: ->
    if @state.edit
      @recordForm()
    else
      @recordRow()

  recordRow: ->
    {tr,td, a, input} = React.DOM
    tr {},
      td {}, @props.record.title
      td {}, @props.record.amount
      td {},
        a
          className: 'btn btn-default'
#         #register callback for toggling edition
          onClick: @handleToggle.bind(this)
          'Edit'
        " "
        a
          className: 'btn btn-danger'
          onClick: @handleDelete.bind(this)
          'Delete'

# #handle toggling
  handleToggle: (e) ->
    e.preventDefault()
    @setState edit: !@state.edit

  recordForm: ->
    {tr,td, a, input} = React.DOM
    tr {},
      td null,
#       #save reference to this element on a record property
        input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.record.title
          ref: (ref) => @titleInput = ref
      td null,
#       #save reference to this element on a record property
        input
          className: 'form-control'
          type: 'number'
          defaultValue: @props.record.amount
          ref: (ref) => @amountInput = ref
      td {},
#       #register callback for edition
        a
          className: 'btn btn-default'
          onClick: @handleEdit.bind(this)
          'Update'
        " "
#       #register callback for toggling edition
        a
          className: 'btn btn-danger'
          onClick: @handleToggle.bind(this)
          'Cancel'


# #the edition handling
  handleEdit: (e)->
    e.preventDefault()
    data =
      title: @titleInput.value
      amount: @amountInput.value
    $.ajax
      method: 'PUT'
      url: "/records/#{ @props.record.id }"
      dataType: 'JSON'
      data:
        record: data
      success: (data) =>
        @setState edit: false
#       #invoke callback to that the parent can also handle the change in the list
        @props.handleEditRecord @props.record, data

  handleDelete: ->
    $.ajax
      method: 'DELETE'
      url: "/records/#{ @props.record.id }"
      dataType: 'JSON'
      success: () =>
        @props.handleDeleteRecord @props.record
