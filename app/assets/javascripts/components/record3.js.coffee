class @Record3 extends React.Component
  render: ->
    {tr,td, a, input} = React.DOM
    tr {},
      td {}, @props.record.title
      td {}, @props.record.amount
#     #new column
      td {},
        a
          className: 'btn btn-danger'
          onClick: @handleDelete.bind(this)
          'Delete'
# #the action for the delete button
  handleDelete: ->
    $.ajax
      method: 'DELETE'
      url: "/records/#{ @props.record.id }"
      dataType: 'JSON'
      success: () =>
        @props.handleDeleteRecord @props.record
