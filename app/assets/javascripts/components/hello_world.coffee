class @HelloWorld extends React.Component
  render: ->
    {div, h1} = React.DOM
    div className: "sample-class-name",
      h1 {}, "Hello #{@props.name}!!!"
