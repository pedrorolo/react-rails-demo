class @HelloWithInput extends React.Component
  constructor: (@props)->
    name = @props.name
    @state = {name: name}

  render: ->
    {div, label, input, br, h1} = React.DOM
    div className: 'my-div-class',
      label
        htmlFor: "helloName"
        "Enter Your name:"
      input
        ref: (ref) =>
          @nameInput = ref
        onChange: @changeName.bind(this)
      h1 {}, "Hello #{@state.name}!!!"

  changeName: ->
    thereIsInput = @nameInput? && @nameInput.value != ""
    newName = if thereIsInput
      @nameInput.value
    else
      @props.name
    @setState name: newName
