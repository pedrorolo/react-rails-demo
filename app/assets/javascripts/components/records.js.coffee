class @Records extends React.Component
  constructor: (@props)->
    @state =
      records: @props.data || []
  render: ->
    ReactCSSTransitionGroup = React.addons.CSSTransitionGroup
    {div, h2, table, thead, tbody, tr, th, br, hline} = React.DOM
    createElement = React.createElement
    div
      className: 'records'
      h2
        className: 'title'
        'Records'
      div
        className: 'row',
        createElement AmountBox,
          type: 'success'
          amount: @credits()
          text: 'Credit'
        createElement AmountBox,
          type: 'danger'
          amount: @debits()
          text: 'Debit'
        createElement AmountBox,
          type: 'info'
          amount: @balance()
          text: 'Balance'
      table
        className: 'table table-bordered',
        thead {},
          tr {},
            th {}, 'Title'
            th {}, 'Amount'
            th {}, 'Actions'
        createElement ReactCSSTransitionGroup,
            {
              component: 'tbody'
              transitionName: 'fade'
              transitionEnterTimeout: 1000
              transitionLeaveTimeout: 1000
            },
            @state.records.map (record) =>
              createElement Record,
                key: record.id
                record: record
                handleDeleteRecord: @deleteRecord.bind(this),
                handleEditRecord: @editRecord.bind(this)
      hline
      createElement RecordForm, handleNewRecord: @addRecord.bind(this)

  credits: ->
    credits = @state.records.filter (val) -> val.amount >= 0
    credits.reduce ((prev, curr) ->
      prev + parseFloat(curr.amount)
    ), 0

  debits: ->
    debits = @state.records.filter (val) -> val.amount < 0
    debits.reduce ((prev, curr) ->
      prev + parseFloat(curr.amount)
    ), 0

  balance: ->
    @debits() + @credits()

  addRecord: (record) ->
    @setState records: @state.records.concat(record)

  deleteRecord: (record)->
    newRecords = @state.records.filter (r) ->
      r.id != record.id
    @setState records: newRecords

  editRecord: (record, data) ->
    newRecords = @state.records.map (r)->
      if record.id == r.id
        data
      else
        r
    @setState records: newRecords
