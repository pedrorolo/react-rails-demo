class @RecordForm extends React.Component
  constructor: (@props) ->
    @state =
      title: @props.title || ''
      amount: @props.amount || ''

  render: ->
    {form,div, input, button} = React.DOM
    form
      className: 'form-inline'
      onSubmit: @handleSubmit.bind(this)
      div
        className: 'form-group col-xs-2'
        input
          type: 'text'
          className: 'form-control'
          placeholder: 'Title'
          name: 'title'
          value: @state.title
          onChange: @handleChange.bind(this)
      div
        className: 'form-group col-xs-2'
        input
          type: 'number'
          className: 'form-control'
          placeholder: 'Amount'
          name: 'amount'
          value: @state.amount
          onChange: @handleChange.bind(this)
      div
        className: 'form-group col-xs-2'
#       #notice when is the button disabled
        button
          type: 'submit'
          className: 'btn btn-primary'
          disabled: !@valid()
          'Create record'

  valid: ->
    @state.title && @state.amount

  handleChange: (e) ->
    name = e.target.name
    @setState "#{name}" : e.target.value

  handleSubmit: (e) ->
    e.preventDefault()
    $.post '/records', { record: @state }, (data) =>
      @props.handleNewRecord data
      @setState title: '', amount: ''
    , 'JSON'
