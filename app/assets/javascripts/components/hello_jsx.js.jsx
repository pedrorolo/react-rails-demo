class HelloJSX extends React.Component {
  render(){
    return (
      <div className='my-div-class'>
        <h1>Hello {this.props.name}</h1>
      </div>
    );
  }
}
